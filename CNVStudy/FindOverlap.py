#!/usr/bin/python

import sys
import optparse

class Region:

    def __init__(self,chr,start,stop,direction,name):
        self.start = int(start)
        if chr.startswith('chr') == False:
            chr = 'chr'+chr
        self.chr = chr
        self.stop = int(stop)
        self.length = self.stop - self.start
        self.direction = direction.replace('CN ','')
        self.overlap = 0
        self.recip = 0
        self.name = name
        if  self.direction != 'Loss' and self.direction != 'Gain':
            print "Not a valid direction: '%s'" %(self.direction)
            sys.exit()

    def FindOverlap(self,chr,start,stop,direction):
        if self.chr != chr:
            return 0
        if self.direction != direction:
            if  direction != 'Loss' and direction != 'Gain':
                print "Not a valid direction: '%s'" %(self.direction)
                sys.exit()
            return 0
        overNo = min(self.stop,int(stop)) - max(int(start),self.start)
        # if overNo < 0 no overlap > gives length
        if overNo < 0:
            return 0
        ratio = (float(overNo)/float(self.length))
        self.overlap += ratio
        return ratio


parser = optparse.OptionParser()
parser.add_option('--hLinesPrimary',
                    dest='hLinesP',
                    help="Set number of header lines to skip PrimaryFile",
                    default=0,
                    type='int')
parser.add_option('--ratioOverlapPrimary', 
                    dest="ratioP",
                    help="Set minimum overlap ratio Primary",
                    default=0.5,
                    type="float")
parser.add_option('--chrPrimary',
                dest="chrColP",
                help= "Set which colum has the chr (1 -based) Primary",
                default=1,
                type="int")
parser.add_option('--startPrimary',
                dest="startColP",
                help= "Set which colum has the start (1 -based) Primary",
                default=2,
                type="int")
parser.add_option('--stopPrimary',
                dest="stopColP",
                help= "Set which colum has the stop (1 -based) Primary",
                default=3,
                type="int")
parser.add_option('--dirColumnPrimary',
                dest="dirColP",
                help= "Set which colum has the Loss/duplication information  (1 -based) Primary",
                default=5,
                type="int")
parser.add_option('--delimPrimary',
                dest="delimP",
                help="set the column delimiter (t for tab or , etc)",
                type="str")
parser.add_option('--nameColumnPrimary',
                dest="nameColP",
                help="set the name column Primary",
                default=4,
                type="int")
parser.add_option('--hLinesBenign',
                    dest='hLinesS',
                    help="Set number of header lines to skip BenignFile",
                    default=1,
                    type='int')
parser.add_option('--ratioOverlapBenign', 
                    dest="ratioS",
                    help="Set minimum overlap ratio Benign",
                    default=0.10,
                    type="float")
parser.add_option('--chrBenign',
                dest="chrColS",
                help= "Set which colum has the chr (1 -based) Benign",
                default=2,
                type="int")
parser.add_option('--startBenign',
                dest="startColS",
                help= "Set which colum has the start (1 -based) Benign",
                default=3,
                type="int")
parser.add_option('--stopBenign',
                dest="stopColS",
                help= "Set which colum has the stop (1 -based) Benign",
                default=4,
                type="int")
parser.add_option('--dirColummnBenign',
                dest="dirColS",
                help= "Set which colum has the Loss/duplication information  (1 -based) Benign",
                default=13,
                type="int")
parser.add_option('--delimBenign',
                dest="delimS",
                help="set the column delimiter (t for tab or , etc)",
                type="str")
parser.add_option('--nameColumnBenign',
                dest="nameColS",
                help="set the name column Benign",
                default=1,
                type="int")
parser.add_option('--primaryFile',dest="primaryFile",help="File containing primary deletions",metavar="FILE")
parser.add_option('--benignFile',dest="begninFile",help="File containing benign deletions",metavar="FILE")
parser.add_option('--patientFile',dest="patientFile",help="File containing patient records",metavar="FILE")
options, remainder = parser.parse_args()

if options.delimP == 't':
    options.delimP = '\t'
if options.delimS == 't':
    options.delimS = '\t'
regionsPrimary = []
regionsBegnin = []
fh = open(options.primaryFile)
for x in range(options.hLinesP):
    fh.readline()
for line in fh:
    line = line.strip('\n')
    line = line.strip('\r')
    cols = line.split(options.delimP)
    chr = cols[options.chrColP-1]
    start = cols[options.startColP-1]
    stop = cols[options.stopColP-1]
    name = cols[options.nameColP-1]
    direction = cols[options.dirColP-1]
    r = Region(chr,start,stop,direction,name)
    regionsPrimary.append(r)

fh.close()

fh = open(options.begninFile)
for x in range(options.hLinesS):
    fh.readline()
for line in fh:
    line = line.strip('\n')
    line = line.strip('\r')
    cols = line.split(options.delimS)
    chr = cols[options.chrColS-1]
    start = cols[options.startColS-1]
    stop = cols[options.stopColS-1]
    name = cols[options.nameColS-1]
    direction = cols[options.dirColS-1]
    r = Region(chr,start,stop,direction,name)
    regionsBegnin.append(r)
fh.close()

# now have the regions files of interest read in (either begnin or path) need to move through the Patient record and find all the regions that meet the threshold

fh.close()

fh = open(options.patientFile)
fh.readline()
currPt = None
patientIntervals = {}
for line in fh:
    line = line.strip('\n')
    line = line.strip('\r')
    cols = line.split('\t') 
    location = cols[1].replace(',','')
    chr,coord = location.split(':')
    start,stop = coord.split('-')
    direction = cols[2].split(' ')[-1]
    name = cols[0]
    if patientIntervals.has_key(cols[0]) == False:
        patientIntervals[cols[0]]=[]
    patientIntervals[cols[0]].append(Region(chr,start,stop,direction,name))
 
header =  "CNV_type\tpt_Name\tpt_CNV_chr\tpt_CNV_start\tpt_CNV_stop\tpt_CNV_direction\tPrimary_location\tP_name\tP_overlap\tP_reciprical_overlap\tPr_direction\tBenign_location\tB_name\tB_overlap\tB_reciprical_overlap\tB_direction\tType_Match"
print header
for i in patientIntervals:
    # i is basically the patient name
    # for each i though there may be many intervals
   
    for pti in patientIntervals[i]:
        for r in regionsPrimary:
            r.FindOverlap(pti.chr,pti.start,pti.stop,pti.direction)
        for b in regionsBegnin:
            b.FindOverlap(pti.chr,pti.start,pti.stop,pti.direction)
        #have gone through and calculated the ratio of overlap with each Primary and Begnin region so those objects have a modified .overlap value
        primaryMatch = []
        benignMatch = []
        for r in regionsPrimary:
            if r.overlap >= options.ratioP:
                r.recip  = (float(r.length)/float(pti.length))
                if r.recip >= options.ratioP:
                    #both the the regular and reciprical overlap meet specification need to report it
                    primaryMatch.append(r)
        for b in regionsBegnin:
            if b.overlap > 0:
                b.recip  = (float(b.length)/float(pti.length))
                if b.recip >= options.ratioS:
                        #same issue need to report an overlap
                        benignMatch.append(b)
                        #print "Benign Overlap\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s " %(r.chr,r.start,r.stop,pti.direction,i,pti.chr,pti.start,pti.stop,r.overlap,brecip,r.name,r.direction)
        
        #have gone through and found all of the overlaps print out a line that summarizes all we have
        string =  "%s\t%s\t%s\t%s\t%s\t"%(i,pti.chr,pti.start,pti.stop,pti.direction)
        if len(primaryMatch) == 0:
            string += "No primary match\tNA\tNA\tNA\tNA\t"
        else:
            for r in primaryMatch:
                string += "%s:%s-%s\t%s\t%s\t%s\t%s\t" %(r.chr,r.start,r.stop,r.name,r.overlap,r.recip,r.direction)
        if len(benignMatch) == 0:
            string += "No benign match\tNA\tNA\tNA\tNA\t"
        else:
            for r in benignMatch:
                string += "%s:%s-%s\t%s\t%s\t%s\t%s\t" %(r.chr,r.start,r.stop,r.name,r.overlap,r.recip,r.direction)
	
	frontname = 'None"  '     
	if len(benignMatch)>0 and len(primaryMatch)>0:
	    frontname = 'Both'
            string += 'Both'
        elif len(benignMatch)>0:
	    frontname = 'Benign Only'
            string += 'Benign Only'
        elif len(primaryMatch)>0:
	    frontname = 'Primary Only'
            string += 'Primary Only'
        else:
	    frontname = 'Secondary CNV'
            string += 'Secondary CNV'
        
	string = frontname + "\t" + string	

	print string
        
        #all done with the overlap counters need to go through and zero out everything
        for r in regionsPrimary:
            r.overlap = 0
            r.recip= 0
        for b in regionsBegnin:
            b.overlap = 0
            b.recip = 0


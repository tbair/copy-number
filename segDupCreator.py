#!/usr/bin/python
import sys
import pickle
import optparse

pickleFile = "./segDupObjects/"
from parsers.Iowa import Iowa
from  parsers.DGV import DGV
from parsers.SegDup import SegDup
from parsers.ISCA import ISCA
from parsers.Conrad import Conrad
from parsers.Cooper import Cooper


parser = optparse.OptionParser()
parser.add_option('--dgv_NumEvents', 
                        dest="dgv_NumEvents",
                    default=0,
                    help="Set minimum number of overlaping events observed Does not yet consider the observation source so studies may not be independent",
                    type="int")
parser.add_option('--dgv_Num05', 
                        dest="dgv_Num05",
                    default=0,
                    help="Set minimum number of overlaping events  observed  at a frequency > 0.5% Does not yet consider the observation source so studies may not be independent",
                    type="int")
parser.add_option('--dgv_Num1', 
                        dest="dgv_Num1",
                    default=0,
                    help="Set minimum number of overlaping events  observed  at a frequency > 1% Does not yet consider the observation source so studies may not be independent",
                    type="int")
parser.add_option('--dgv_Num2', 
                        dest="dgv_Num2",
                    default=0,
                    help="Set minimum number of overlaping events  observed  at a frequency > 2% Does not yet consider the observation source so studies may not be independent",
                    type="int")

parser.add_option('--dgv_NumControls', 
                    dest="dgv_NumControls",
                    default=0,
                    help="Set number of DGV controls",
                    type="int")
parser.add_option('--dgv_StudyType', 
                    dest="dgv_StudyType",
                    default='Any',
                    help="Set DGV StudyType (Any (default) ,Microarray or Other)",
                    )
parser.add_option('--dgv_freqDup', 
                    dest="dgv_freqDup",
                    help="Set frequency of Duplication 0-1",
                    default=0.00,
                    type="float")

parser.add_option('--dgv_freqDel', 
                    dest="dgv_freqDel",
                    help="Set frequency of Deletion 0-1",
                    default=0.00,
                    type="float")
parser.add_option('--dgv_percentDup', 
                    dest="dgv_percentDup",
                    help="Set frequency of Duplication 0-100. Do not combine with freqDup",
                    default=0.00,
                    type="float")

parser.add_option('--dgv_percentDel', 
                    dest="dgv_percentDel",
                    help="Set frequency of Deletion 0-100. Do not combine with freqDel",
                    default=0.00,
                    type="float")

parser.add_option('--dgv_reqDup', 
                    dest="dgv_reqDup",
                    action="store_true",
                    help="Reqire one duplication default False",
                    default=False,
                    )

parser.add_option('--dgv_reqDel', 
                    dest="dgv_reqDel",
                    action="store_true",
                    help="Reqire one deletion default False",
                    default=False,
                    )
parser.add_option('--dgv_collapse', 
                    dest="dgv_collapse",
                    action="store_true",
                    help="Include some DGV data",
                    default=True,
                    )

parser.add_option('--dgv', 
                    dest="dgv",
                    action="store_true",
                    help="Include some DGV data",
                    default=False,
                    )

parser.add_option('--segDup', 
                    dest="segDup",
                    action="store_true",
                    help="Include all the segDup data",
                    default=False,
                    )
parser.add_option('--iowa', 
                    dest="iowa",
                    action="store_true",
                    help="Include some of the Iowa data",
                    default=False,
                    )
parser.add_option('--iowa_Dup', 
                    dest="iowa_Dup",
                    action="store_true",
                    help="Include Iowa duplications",
                    default=False,
                    )
parser.add_option('--iowa_Del', 
                    dest="iowa_Del",
                    action="store_true",
                    help="Include Iowa deletions",
                    default=False,
                    )


parser.add_option('--iowa_freq', 
                    dest="iowa_freq",
                    help="Set minimum frequency for Iowa data (already filtered to 0.02) Do not set both frequency and percentage",
                    default=0.0,
                    type="float")
parser.add_option('--iowa_collapse', 
                    dest="iowa_collapse",
                    action="store_true",
                    help="Collapse the iowa data to have 1 entry per overlapping region",
                    default=False,
                    )

parser.add_option('--isca', 
                    dest="isca",
                    action="store_true",
                    help="Include some of the ISCA data",
                    default=False,
                    )

parser.add_option('--isca_collapse', 
                    dest="isca_collapse",
                    action="store_true",
                    help="Collapse the isca data to have 1 entry per overlapping region",
                    default=False,
                    )

parser.add_option('--isca_All', 
                    dest="isca_All",
                    action="store_true",
                    help="Get ISCA calls regardless of annotation value. This overides any isca_Benign or similar flags",
                    default=False,
                    )

parser.add_option('--isca_Benign', 
                    dest="isca_Benign",
                    action="store_true",
                    help="ISCA calls can be annotated benign",
                    default=False,
                    )
parser.add_option('--isca_LBenign', 
                    dest="isca_LBenign",
                    action="store_true",
                    help="ISCA calls can be annotated likely benign",
                    default=False,
                    )

parser.add_option('--isca_Uncertain', 
                    dest="isca_Uncertain",
                    action="store_true",
                    help="ISCA calls can be annotated Uncertain",
                    default=False,
                    )
parser.add_option('--isca_LPath', 
                    dest="isca_LPath",
                    action="store_true",
                    help="ISCA calls can be annotated likely path",
                    default=False,
                    )
parser.add_option('--isca_Path', 
                    dest="isca_Path",
                    action="store_true",
                    help="ISCA calls can be annotated  path",
                    default=False,
                    )
parser.add_option('--isca_Del', 
                    dest="isca_Del",
                    action="store_true",
                    help="ISCA calls can only be deletions",
                    default=False,
                    )
parser.add_option('--isca_Dup', 
                    dest="isca_Dup",
                    action="store_true",
                    help="ISCA calls can only be duplications",
                    default=False,
                    )
parser.add_option('--conrad', 
                    dest="conrad",
                    action="store_true",
                    help="Include some of the Conrad data",
                    default=False,
                    )

parser.add_option('--conrad_Valid', 
                    dest="conrad_Valid",
                    action="store_true",
                    help="Include only the validated data",
                    default=False,
                    )
parser.add_option('--conrad_CEUFreq', 
                    dest="conrad_CEUFreq",
                    help="Set minimum frequency for Conrad CEU frequency (0-1)",
                    default=0.0,
                    type="float")

parser.add_option('--conrad_YRIFreq', 
                    dest="conrad_YRIFreq",
                    help="Set minimum frequency for Conrad YRI frequency (0-1)",
                    default=0.0,
                    type="float")

parser.add_option('--conrad_reqDup', 
                    dest="conrad_reqDup",
                    action="store_true",
                    help="Reqire Conrad duplication default False",
                    default=False,
                    )

parser.add_option('--conrad_reqDel', 
                    dest="conrad_reqDel",
                    action="store_true",
                    help="Reqire Conrad deletion default False",
                    default=False,
                    )

parser.add_option('--conrad_collapse', 
                    dest="conrad_collapse",
                    action="store_true",
                    help="Collapse the conrad data to have 1 entry per overlapping region",
                    default=False,
                    )
parser.add_option('--cooper', 
                    dest="cooper",
                    action="store_true",
                    help="Include some of the Cooper data",
                    default=False,
                    )
parser.add_option('--cooper_IncludeGenes', 
                    dest="cooper_Genes",
                    action="store_true",
                    help="Include Cooper data that has entry in 'Genes' file",
                    default=False,
                    )

parser.add_option('--cooper_ReqGenes', 
                    dest="cooper_ReqGenes",
                    action="store_true",
                    help="Require Cooper data that has entry in 'Genes' file (default False)",
                    default=False,
                    )
parser.add_option('--cooper_reqDup', 
                    dest="cooper_reqDup",
                    action="store_true",
                    help="Reqire Cooper duplication default False",
                    default=False,
                    )

parser.add_option('--cooper_reqDel', 
                    dest="cooper_reqDel",
                    action="store_true",
                    help="Reqire Cooper deletion default False",
                    default=False,
                    )
parser.add_option('--cooper_collapse', 
                    dest="cooper_collapse",
                    action="store_true",
                    help="Collapse the cooper data to have 1 entry per overlapping region",
                    default=False,
                    )


options, remainder = parser.parse_args()
if options.dgv_freqDup and options.dgv_percentDup:
    print "Cannot set both frequency and percentage options"
    parser.print_help()
    sys.exit()
if options.dgv_freqDel and options.dgv_percentDel:
    print "Cannot set both frequency and percentage options"
    parser.print_help()
    sys.exit()
if options.dgv == True:
    DGVObjects = pickle.load(open(pickleFile+"DGV.pickle","rb"))
    if options.dgv_percentDup:
        options.dgv_freqDup = options.dgv_percentDup/100.0
    if options.dgv_percentDup:
        options.dgv_freqDel = options.dgv_percentDel/100.0
    output = []
    for d in DGVObjects:
        if options.dgv_Num05:
            if d.numOverlaps_05 < options.dgv_Num05:
                continue
        if options.dgv_Num1:
            if d.numOverlaps_1 < options.dgv_Num1:
                continue
        if options.dgv_Num2:
            if d.numOverlaps_2 < options.dgvNum2:
                continue
        if options.dgv_NumEvents:
            if d.numOverlaps_0 < options.dgv_Num0:
                continue
        if d.meetCriteria(
            options.dgv_StudyType,
            options.dgv_NumControls,
            options.dgv_freqDup,
            options.dgv_freqDel,
            options.dgv_reqDup,
            options.dgv_reqDel
            ) == True:
                output.append(d)

#I just can't follow the whole "collapse" code - I'm not very good with hash/dictionaries

    if options.dgv_collapse:
        #go through and create an ordered dictionary of starts and stops
        #create list of minimal start/stops
        #merge all d by minimal list
        chroms = {}
        for d in output:
            chroms[d.chr] = 1
        for c in chroms:
            hash = {}
            for d in output:
                if d.chr == c:
                    if hash.has_key(d.start) == False:
                        hash[d.start] = 'start'
                    hash[d.stop] ='end' #even if you have a start there replace with end
            ordered = hash.keys()
            ordered.sort()
            intervals = []
            start = None
            state = None
            lastStop = None
            for o in ordered:
                if start == None:
                    start = o
                    state = 'start'
                elif hash[o] == 'end':
                    lastStop = o
                    state = 'end'
                elif hash[o] == 'start' and state == 'end':
                    intervals.append({'start':start,'end':lastStop,'data':[]})
                    start = o
                    lastStop = o
            intervals.append({'start':start,'end':ordered[-1],'data':[]})#close out intervals
            for i in intervals:
                for d in output:
                    if d.chr == c:
                        #detect overlap
                        overlap = min(i['end'],d.stop) - max(d.start,i['start'])
                        if overlap >= 0:
                                i['data'].append(d.output().split('\t')[3])
            for i in intervals:
                print "%s\t%s\t%s\t%s"%(c,i['start'],i['end'],i['data'])
                

    else:
        for d in output:
            print d.output()



if options.segDup == True:
   SEGDupObjects = pickle.load(open(pickleFile+"SEGDup.pickle","rb"))
   for s in SEGDupObjects:
    print s.output()
if options.iowa == True:
   IowaObjects = pickle.load(open(pickleFile+"Iowa.pickle","rb"))
   for i in IowaObjects:
       if i.meetCriteria(
        options.iowa_Del,
        options.iowa_Dup,
        options.iowa_freq,
        ):
            print i.output()
if options.isca == True:
    ISCAObjects = pickle.load(open(pickleFile+"ISCA.pickle","rb"))
    output = []
    for i in ISCAObjects:
        if i.valid() == True:
            flag = True
            if options.isca_Benign == True and i.hasKey('Benign') == False:
                flag = False
            if options.isca_Path == True and i.hasKey('Path') == False:
                flag = False
            if options.isca_Uncertain == True and i.hasKey('Uncertain') == False:
                flag = False
            if options.isca_LBenign == True and i.hasKey('LikelyBenign') == False:
                flag = False
            if options.isca_LPath == True and i.hasKey('LikelyPath') == False:
                flag = False
            if options.isca_Del == True and i.Del == False:
                flag = False
            if options.isca_Dup == True and i.Dup == False:
                flag = False
            if options.isca_All == True:
                flag = True
            if flag == True:
                output.append(i)
    if options.isca_collapse:
        #go through and create an ordered dictionary of starts and stops
        #create list of minimal start/stops
        #merge all d by minimal list
        chroms = {}
        for d in output:
            chroms[d.chr] = 1
        for c in chroms:
            hash = {}
            for d in output:
                if d.chr == c:
                    if hash.has_key(d.start) == False:
                        hash[d.start] = 'start'
                    hash[d.stop] ='end' #even if you have a start there replace with end
            ordered = hash.keys()
            ordered.sort()
            intervals = []
            start = None
            state = None
            lastStop = None
            for o in ordered:
                if start == None:
                    start = o
                    state = 'start'
                elif hash[o] == 'end':
                    lastStop = o
                    state = 'end'
                elif hash[o] == 'start' and state == 'end':
                    intervals.append({'start':start,'end':lastStop,'data':[]})
                    start = o
                    lastStop = o
            intervals.append({'start':start,'end':ordered[-1],'data':[]})#close out intervals
            for i in intervals:
                for d in output:
                    if d.chr == c:
                        #detect overlap
                        overlap = min(i['end'],d.stop) - max(d.start,i['start'])
                        if overlap >= 0:
                                i['data'].append(d.output().replace('\n',' ').split('\t')[3])
            for i in intervals:
                print "%s\t%s\t%s\t%s"%(c,i['start'],i['end'],i['data'])
                

    else:
        for d in output:
            print d.output()

    
if options.conrad == True:
    ConradObjects = pickle.load(open( pickleFile+"Conrad.pickle","rb"))
    output = []
    for c in ConradObjects:
        flag = True
        if options.conrad_Valid == True:
            if c.validated == 0:
                flag = False
        if c.ceuFreq < options.conrad_CEUFreq:
            flag = False
        if c.yriFreq < options.conrad_YRIFreq:
            flag = False
        if options.conrad_reqDup:
            if c.Dup == False:
                flag = False
        if options.conrad_reqDel:
            if c.Del == False:
                flag = False
        if flag == True:    
            output.append(c)
    if options.conrad_collapse:
        #go through and create an ordered dictionary of starts and stops
        #create list of minimal start/stops
        #merge all d by minimal list
        chroms = {}
        for d in output:
            chroms[d.chrom] = 1
        for c in chroms:
            hash = {}
            for d in output:
                if d.chrom == c:
                    if hash.has_key(d.start) == False:
                        hash[d.start] = 'start'
                    hash[d.stop] ='end' #even if you have a start there replace with end
            ordered = hash.keys()
            ordered.sort()
            intervals = []
            start = None
            state = None
            lastStop = None
            for o in ordered:
                if start == None:
                    start = o
                    state = 'start'
                elif hash[o] == 'end':
                    lastStop = o
                    state = 'end'
                elif hash[o] == 'start' and state == 'end':
                    intervals.append({'start':start,'end':lastStop,'data':[]})
                    start = o
                    lastStop = o
            intervals.append({'start':start,'end':ordered[-1],'data':[]})#close out intervals
            for i in intervals:
                for d in output:
                    if d.chrom == c:
                        #detect overlap
                        overlap = min(i['end'],d.stop) - max(d.start,i['start'])
                        if overlap >= 0:
                                i['data'].append(d.output().replace('\n',' ').split('\t')[3])
            for i in intervals:
                print "%s\t%s\t%s\t%s"%(c,i['start'],i['end'],i['data'])
                


if options.cooper == True:
    CooperObjects = pickle.load(open( pickleFile+"Cooper.pickle","rb"))
    for c in CooperObjects:
        flag = True
        if options.cooper_Genes == False:
            if c.hasGeneInfo == False:
                flag = False
        if options.cooper_ReqGenes == True:
            if c.hasGeneInfo() == False:
                flag = False
        if options.cooper_reqDup:
            if c.Dup == False:
                flag = False
        if options.cooper_reqDel:
            if c.Del == False:
                flag = False
        if flag == True:
            print c.output()

    
    




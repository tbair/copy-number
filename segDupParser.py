#!/usr/bin/python
import sys
import pickle
import optparse
pickleFile = "./segDupObjects/"
from parsers.Iowa import Iowa
from  parsers.DGV import DGV
from parsers.SegDup import SegDup
from parsers.ISCA import ISCA
from parsers.Conrad import Conrad
from parsers.Cooper import Cooper

parser = optparse.OptionParser()

parser.add_option('--dgv', 
                    dest="dgv",
                    action="store_true",
                    default=False,
                    help="parse and reset the DGV file",
                    )

parser.add_option('--segDup', 
                    dest="segDup",
                    action="store_true",
                    default=False,
                    help="parse and reset the segDup file",
                    )
parser.add_option('--iowa', 
                    dest="iowa",
                    action="store_true",
                    default=False,
                    help="parse and reset the Iowa file",
                    )
parser.add_option('--isca', 
                    dest="isca",
                    action="store_true",
                    default=False,
                    help="parse and reset the isca file",
                    )
parser.add_option('--cooper', 
                    dest="cooper",
                    action="store_true",
                    default=False,
                    help="parse and reset the Cooper file",
                    )
parser.add_option('--conrad', 
                    dest="conrad",
                    action="store_true",
                    default=False,
                    help="parse and reset the Conrad file",
                    )

options, remainder = parser.parse_args()


if options.dgv == True:
    DGVObjects = []
    fh = open(DGV.fileLocation)
    fh.readline()
    sucess = 0
    fail = 0
    for line in fh:
        try:
            x = DGV(line)
            DGVObjects.append(x)
            sucess += 1
        except Exception, e:
            print "Unable to parse %s " %(e)
            print line
            fail +=1
    fh.close()
    print "DGV %s succeded %s failed to parse" %(sucess,fail)
    #need to go through and figure out how many overlaps there are for each entry
    for d in DGVObjects:
        counter_0 = 0
        counter_05 = 0
        counter_1 = 0
        counter_2 = 0
        for x in DGVObjects:
            if d.overLap(x.chr,x.start,x.stop) == True:
                counter_0 +=1
                if x.freq > 0.005:
                    counter_05 +=1
                if x.freq > 0.01:
                    counter_1 +=1
                if x.freq > 0.02:
                    counter_2 +=1
        d.numOverlaps_0 = counter_0
        d.numOverlaps_05 = counter_05
        d.numOverlaps_1 = counter_1
        d.numOverlaps_2 = counter_2
        
    pickle.dump(DGVObjects,open(pickleFile+"DGV.pickle","wb"))

if options.segDup == True:
    SEGDupObjects = []
    fh = open(SegDup.fileLocation)
    sucess = 0
    fail = 0
    for line in fh:
        try:
            x = SegDup(line)
            SEGDupObjects.append(x)
            sucess += 1
        except Exception, e:
            print "Unable to parse %s " %(e)
            print line
            fail +=1
    print "SegDup %s succeded %s failed to parse" %(sucess,fail)
    pickle.dump(SEGDupObjects,open(pickleFile+"SEGDup.pickle","wb"))
    fh.close()
if options.iowa == True:
    IowaObjects = []
    fh = open(Iowa.fileLocation)
    sucess = 0
    fail = 0
    for line in fh:
        try:
            x = Iowa(line)
            IowaObjects.append(x)
            sucess += 1
        except Exception, e:
            print "Unable to parse %s " %(e)
            print line
            fail +=1
    print "Iowa %s succeded %s failed to parse" %(sucess,fail)
    pickle.dump(IowaObjects,open(pickleFile+"Iowa.pickle","wb"))
    fh.close()
if options.isca == True:
    ISCAObjects = []
    ISCAHash = {}
    fh = open(ISCA.fileLocation)
    sucess = 0
    fail = 0
    x = None
    for line in fh:
        try:
            x = ISCA(line)
            ISCAHash[x.getID()] = x
            sucess += 1
        except Exception, e:
            print "Unable to parse %s " %(e)
            print line
            fail +=1
    print "ISCA %s succeded %s failed to parse" %(sucess,fail)
    x.processExtraFiles(ISCAHash)
    for x in ISCAHash:
        ISCAObjects.append(ISCAHash[x])
    #need to go through and figure out how many overlaps there are for each entry
    for d in ISCAObjects:
        counter_0 = 0
        for x in ISCAObjects:
            if d.overLap(x.chr,x.start,x.stop) == True:
                counter_0 +=1
        d.numOverlaps = counter_0
    pickle.dump(ISCAObjects,open(pickleFile+"ISCA.pickle","wb"))
    #pickle.dump(ISCAHash,open(pickleFile+"ISCAH.pickle","wb"))
 
if options.conrad == True:
    ConradObjects = []
    fh = open(Conrad.fileLocation_gain)
    sucess = 0
    fail = 0
    for line in fh:
        try:
            x = Conrad(line,'gain')
            ConradObjects.append(x)
            sucess += 1
        except Exception, e:
            print "Unable to parse %s " %(e)
            print line
            fail +=1
    print "Conrad Gains %s succeded %s failed to parse" %(sucess,fail)
    fh.close()

    fh = open(Conrad.fileLocation_loss)
    sucess = 0
    fail = 0
    for line in fh:
        try:
            x = Conrad(line,'loss')
            ConradObjects.append(x)
            sucess += 1
        except Exception, e:
            print "Unable to parse %s " %(e)
            print line
            fail +=1
    print "Conrad Loses %s succeded %s failed to parse" %(sucess,fail)
    fh.close()
    pickle.dump(ConradObjects, open( pickleFile+"Conrad.pickle","wb"))
if options.cooper == True:
    CooperObjects = []
    fh = open(Cooper.fileLocation)
    sucess = 0
    fail = 0
    x = None
    for line in fh:
        try:
            x = Cooper(line,'loss')
            CooperObjects.append(x)
            sucess += 1
        except Exception, e:
            print "Unable to parse %s " %(e)
            print line
            fail +=1
    print "Cooper Loses %s succeded %s failed to parse" %(sucess,fail)
    fh.close()
    x.processExtraFiles(CooperObjects)
    sucess = 0
    fail = 0
    for x in CooperObjects:
        if x.hasGeneInfo() == True:
            sucess += 1
        else:
            fail += 1
    print "Cooper gene info  %s succeded %s failed to parse" %(sucess,fail)
    pickle.dump(CooperObjects, open(pickleFile+"Cooper.pickle","wb"))


#take the object and store for later queries


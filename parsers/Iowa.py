
class Iowa:

    fileLocation = "./IOWA_Data_allhg18/IOWA_2percent_gains_losses_nopeaks.txt"
    def __init__(self,line):
        cols = line.split('\t')
        (chr,pos) = cols[0].split(':')
        (start,stop) = pos.split('-')
        start = start.replace(',','')
        stop = stop.replace(',','')
        self.chr = chr
        self.start = int(start)
        self.stop = int(stop)
        self.frequency =float(cols[6])
        if cols[3] == 'CN Loss':
            self.Del = True
            self.Dup = False
        elif cols[3] == 'CN Gain':
            self.Del = False
            self.Dup = True
        else:
            raise Exception ('CN parse error')
    def meetCriteria(self,Del,Dup,minfreq):
        if self.frequency < minfreq:
            return False
        if self.Del == Del:
            return True
        if self.Dup == Dup:
            return True
    def output(self):
        type = ''
        if self.Del == True:
            type += 'Deletion'
        if self.Dup == True:
            type += 'Duplication'
            
        return "%s\t%s\t%s\tIOWA:%s Type:%s" %(self.chr,self.start,self.stop,self.frequency,type)




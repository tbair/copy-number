import re

class Conrad:
    fileLocation_gain = "./Conrad_Data_allhg18/IowaNimb42Val&Nonval_Gains.txt"
    fileLocation_loss = "./Conrad_Data_allhg18/IowaNimb42Val&Nonval_Losses.txt"
    numbers = re.compile('CEUFreq (\d+) YRIFreq (\d+) Validated:(\d+)')
    def __init__(self,line,type):
        line = line.strip('\n')
        line = line.strip('\r')
        cols = line.split('\t')
        self.chrom = 'chr'+cols[1]
        self.start = int(cols[2])
        self.stop = int(cols[3])
        self.name = cols[4]
        self.type = type
        if type == 'gain':
            self.Dup = True
            self.Del = False
        if type ==  'loss':
            self.Dup = False
            self.Del = True
        nums = Conrad.numbers.search(cols[4])
        self.ceuFreq = (int(nums.group(1))/20.0)
        self.yriFreq = (int(nums.group(2))/20.0)
        self.validated = int(nums.group(3))

    def overLap (self,chr,start,stop):
        if stop < start: #force typical direction
            temp = stop
            stop = start
            start = temp
        if self.chrom != chr:
            return False
        if self.start <= start and self.stop > stop:
            return True
        else:
            return False
    def output(self):
        return "%s\t%s\t%s\tConrad:%s:%s" %(self.chrom,self.start,self.stop,self.type,self.name)

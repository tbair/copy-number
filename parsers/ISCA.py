
class ISCA:

    fileLocation = "./ISCA_Data/ISCA_DETAILS_supporting_variants_for_nstd37.csv"
    additionalInfo = {
    "./ISCA_Data/ISCA_Benign_CNVs_hg18_2012-06-14.bed":"Benign",
    "./ISCA_Data/ISCA_Likely_Benign_CNVs_hg18_2012-06-14.bed":"LikelyBenign",
    "./ISCA_Data/ISCA_Likely_Pathogenic_CNVs_hg18_2012-06-14.bed":"LikelyPath",
    "./ISCA_Data/ISCA_Pathogenic_CNVs_hg18_2012-06-14.bed":"Path",
    "./ISCA_Data/ISCA_Uncertain_CNVs_hg18_2012-06-14.bed":"Uncertain",
    }
    #Note that this is a bit different parser as there is a details file (may not be all inclusive) and then separate files for the different mutation types which need to also be loaded
    def __init__(self,line):
        cols = line.split(',')
        self.ID = cols[1].replace('"','')
        self.fullID = None
        self.method = cols[5].replace('"','')
        self.values = {}
        self.chr = None
        self.start = None
        self.stop = None
        self.path = None
        self.numOverlaps = None
        self.CVtype = None
        if cols[3] == '"Copy number loss"':
            self.Del = True
            self.Dup = False
            self.CVtype = 'loss'
        elif cols[3] == '"Copy number gain"':
            self.Del = False
            self.Dup = True
            self.CVtype = 'gain'
        else:
            print cols[3]
            raise Exception ('CN parse error')
    def processExtraFiles(self,hash):
        for f in ISCA.additionalInfo:
            fh = open(f)
            for line in fh:
                cols = line.split('\t')
                id = cols[3].split('_')[0]
                try:
                    hash[id].chr = cols[0]
                    hash[id].start = int(cols[1])
                    hash[id].stop = int(cols[2])
                    hash[id].addBedInfo(ISCA.additionalInfo[f],'True')
                    hash[id].fullID = cols[3]
                except Exception, e:
                    print e
            fh.close()
    def hasKey(self,key):
        if self.values.has_key(key) == True:
            return True
        return False
        
    def valid(self):
        if self.chr == None:
            return False
        else:
            return True
    def data(self):
        return (self.ID,self.chr,self.start,self.stop,self.values)
                
    def getID(self):
        return self.ID
    def matchID (self, ID):
        if ID == self.ID:
            return True
        else:
            return False
    def addBedInfo(self,key,value):
        self.values[key] = value
    def overLap (self,chr,start,stop):
        if stop < start: #force typical direction
            temp = stop
            stop = start
            start = temp
        if self.chr != chr:
            return False
        if self.start == start and self.stop == stop:
            return True
        elif self.stop <= start and self.stop >= start and self.stop >= stop:
            return True
        else:
            return False

    def output(self):
        addValues = ""
        for x in self.values:
            addValues += x+" "

        return "%s\t%s\t%s\tOverlaps: %s ISCA:%s Values:%s Type:%s" %(self.chr,self.start,self.stop,self.numOverlaps,self.fullID,addValues,self.CVtype)




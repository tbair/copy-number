from Iowa import Iowa

class DGV:
    fileLocation =  "./DGV_Data_allhg18/DGV_variation.hg18.v10.nov.2010.txt"

    def __init__(self,line):
        cols = line.split('\t')
        numControls = cols[16].split(' ')[0]# plus regex to pull number
        numControls = numControls.replace(',','')
        self.chr =cols[2]
        self.start = int(cols[3])
        self.stop = int(cols[4])
        self.type = cols[5]
        self.numControls = int(numControls)
        self.numOverlaps_0 = 0
        self.numOverlaps_05 = 0
        self.numOverlaps_1 = 0
        self.numOverlaps_2 = 0
        if cols[12] == '' or cols[12] == ' ' or cols[12] == 'NA':
            self.numDup = 0
        else:
            self.numDup = int(cols[12])
        if cols[13] == '' or cols[13] == ' ' or cols[13] == 'NA':
            self.numDel = 0
        else:
            self.numDel = int(cols[13])
        self.relivantAnnotation = cols[9]
        self.study = cols[9]
        self.id = cols[0]
        self.studyMethod = cols[11]
        if self.studyMethod.startswith('Affymetrix'):
            self.studyMethod = 'Microarray'
        elif self.studyMethod.startswith('Illumina'):
            self.studyMethod = 'Microarray'
        elif self.studyMethod.startswith('Custom'):
            self.studyMethod = 'Microarray'
        elif self.studyMethod.startswith('Agilent'):
            self.studyMethod = 'Microarray'
        elif self.studyMethod.startswith('NimbleGen'):
            self.studyMethod = 'Microarray'
        else:
            self.studyMethod = 'Other'
        self.freqDup = float(self.numDup)/float(self.numControls)
        self.freqDel = float(self.numDel)/float(self.numControls)


#NOTE SURE WHAT THIS LINE IS DOING BELOW - and in the parser it comes into play significantly in determining whether or not there is a count for 0.005, 0.01, or 0.02
        if self.freqDup > self.freqDel:
            self.freq = self.freqDup
        else:
            self.freq = self.freqDel


    def overLap (self,chr,start,stop):
        if stop < start: #force typical direction
            temp = stop
            stop = start
            start = temp
        if self.chr != chr:
            return False
        if self.start == start and self.stop == stop:
            return True
        elif start <= self.stop and self.stop >= start and self.stop >= stop: #DOES THIS LAST CRITERIA NEED TO BE IN HERE???  NOT SURE WHAT IT IS DOING.
            return True
        else:
            return False


    def meetCriteria(self,type,numControls,frequencyDup,frequenceDel, oneDup,oneDel):
        if type == 'Any':
            pass
        elif self.studyMethod != type:
            return False
        if self.numControls < numControls:
            return False
        if self.freqDup < frequencyDup:
            return False
        if self.freqDel < frequenceDel:
            return False
        if oneDup == True and self.numDup == 0:
            return False
        if oneDel == True and self.numDel == 0:
            return False
        return True

    def output(self):
        return "%s\t%s\t%s\tDGV:%s FreqDup:%s FreqDel:%s #Observed 05: %s 1: %s 2: %s  Method:%s Reference:%s" %(self.chr,self.start,self.stop,self.id,self.freqDup,self.freqDel,self.numOverlaps_05,self.numOverlaps_1,self.numOverlaps_2,self.studyMethod,self.study)


class SegDup:
    fileLocation = "./SegDup_Data_allhg18/UCSC_segdup_tables_download.bed"
    def __init__(self,line):
        cols = line.split('\t')
        self.chrom = cols[0]
        self.start = int(cols[2])
        self.stop = int(cols[2])
        self.name = cols[3]
    def overLap (self,chr,start,stop):
        if stop < start: #force typical direction
            temp = stop
            stop = start
            start = temp
        if self.chrom != chr:
            return False
        if self.start <= start and self.stop > stop:
            return True
        else:
            return False
    def output(self):
        return "%s\t%s\t%s\tSEGDUP:%s" %(self.chrom,self.start,self.stop,self.name)

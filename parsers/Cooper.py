
class Cooper:
    fileLocation = "./Cooper_Data/Cooper_ALL_supporting_variants_for_nstd54.csv"
    additionalInfo = {
    "./Cooper_Data/Cooper_Gene_Dups_Nexus_hg18.txt":"Gain",
    "./Cooper_Data/Cooper_Gene_Dels_Nexus_hg18.txt":"Loss",
    }
    def __init__(self,line,type):
        cols = line.split(',')
        if cols[11] !=  '"NCBI36 (hg18)"':
            raise Exception('hg19')
        else:
            
            self.chrom = 'chr'+cols[13].replace('"','')
            self.start = int(cols[14].replace('"',''))
            self.stop = int(cols[15].replace('"',''))
            self.name = cols[1].replace('"','')
            self.Gene = None
            type = cols[3].replace('"','')
            if type == 'Gain':
                self.Dup = True
                self.Del = False
            if type ==  'Loss':
                self.Dup = False
                self.Del = True
    def processExtraFiles(self,list):
        for f in Cooper.additionalInfo:
            fh = open(f)
            #have to go through the previous objects and see if they overlap with this gene, if they are contained within this gene add info
            for line in fh:
                #get the gene position
                line = line.strip('\n')
                cols = line.split('\t')
                geneChr = None
                geneStart = None
                geneStop = None
                try:
                    geneChr = cols[2]
                    geneStart = int(cols[3])
                    geneStop = int(cols[4])
                    info = cols[0]
                except:
                    pass
                if geneStart != None:
                    for l in list:
                        if l.overLap(geneChr,geneStart,geneStop) == True:
                            l.Gene = info

            fh.close()
    def overLap (self,chr,start,stop):
        if stop < start: #force typical direction
            temp = stop
            stop = start
            start = temp
        if self.chrom != chr:
            return False
        if self.start >= start and self.stop <= stop:
            return True
        else:
            return False
    def hasGeneInfo(self):
        if self.Gene != None:
            return True
        else:
            return False

    def validObject(self):
        try:
            if self.chrom != None:
                return True
        except:
            return False
    def output(self):
        return "%s\t%s\t%s\tCOOPER:%s SigE:%s" %(self.chrom,self.start,self.stop,self.name,self.Gene)
